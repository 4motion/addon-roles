<?php

namespace Onyx\Role;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Onyx\Product\Models\ProductTag;
use Onyx\Role\Models\Role;
use Illuminate\Support\Facades\Auth;
use Onyx\Log\LogController;
use Onyx\Tag\Models\Tag;
use Onyx\User\Models\User;


class RoleController extends Controller
{

    /**
     * Ajax function of delete role.
     */
    public function ajaxDeleteRole(Request $request)
    {

        $result = false;

        $role = Role::where('id', $request->get("roleId"))->first();

//      Change users with this role to default
        if(User::where("role_id", $role->id)->get()) {
            User::where("role_id", $role->id)->update([
               "role_id" => 2
            ]);
        }

        if($role->delete()){

            $result = true;
            LogController::insert($request->get("userId"), 4, 'Role ID '. $role->id .' successfully deleted.');
        }

        return $result;

    }

    /**
     * Index page of Roles.
     */
    public function index(): Factory|View|Application
    {
        $pageTitle = __('role::role.rolesIndexSub');
        $pageSub = trans('rolesIndexSub');

        $roles = Role::orderBy("name_cs")->get();

        return view('roles::index', compact('pageTitle', 'pageSub', 'roles'));
    }


    /**
     * Create page of Roles.
     */
    public function create(): Factory|View|Application
    {

        $pageTitle = __('role::role.rolesCreateSub');
        $pageSub = trans('rolesCreateSub');


        return view('roles::create', compact('pageTitle', 'pageSub'));
    }


    /**
     * Store method of Roles.
     */
    public function store(Request $request)
    {

        $this->validateRole($request);

        $role = new Role($request->only('name_cs', 'name_en'));

            $role->product_index = $request->get("product_index") == 'on' ? 1 : 0;
            $role->product_create = $request->get("product_create") == 'on' ? 1 : 0;
            $role->product_edit = $request->get("product_edit") == 'on' ? 1 : 0;
            $role->product_delete = $request->get("product_delete") == 'on' ? 1 : 0;

            $role->category_index = $request->get("category_index") == 'on' ? 1 : 0;
            $role->category_create = $request->get("category_create") == 'on' ? 1 : 0;
            $role->category_edit = $request->get("category_edit") == 'on' ? 1 : 0;
            $role->category_delete = $request->get("category_delete") == 'on' ? 1 : 0;

            $role->tag_index = $request->get("tag_index") == 'on' ? 1 : 0;
            $role->tag_create = $request->get("tag_create") == 'on' ? 1 : 0;
            $role->tag_edit = $request->get("tag_edit") == 'on' ? 1 : 0;
            $role->tag_delete = $request->get("tag_delete") == 'on' ? 1 : 0;

            $role->user_index = $request->get("user_index") == 'on' ? 1 : 0;
            $role->user_create = $request->get("user_create") == 'on' ? 1 : 0;
            $role->user_edit = $request->get("user_edit") == 'on' ? 1 : 0;
            $role->user_delete = $request->get("user_delete") == 'on' ? 1 : 0;

            $role->role_index = $request->get("role_index") == 'on' ? 1 : 0;
            $role->role_create = $request->get("role_create") == 'on' ? 1 : 0;
            $role->role_edit = $request->get("role_edit") == 'on' ? 1 : 0;
            $role->role_delete = $request->get("role_delete") == 'on' ? 1 : 0;

            $role->page_index = $request->get("page_index") == 'on' ? 1 : 0;
            $role->page_create = $request->get("page_create") == 'on' ? 1 : 0;
            $role->page_edit = $request->get("page_edit") == 'on' ? 1 : 0;
            $role->page_delete = $request->get("page_delete") == 'on' ? 1 : 0;

            $role->report_index = $request->get("report_index") == 'on' ? 1 : 0;

            $role->import_index = $request->get("import_index") == 'on' ? 1 : 0;
            $role->import_create = $request->get("import_create") == 'on' ? 1 : 0;

            $role->setting_index = $request->get("setting_index") == 'on' ? 1 : 0;
            $role->log_index = $request->get("log_index") == 'on' ? 1 : 0;

        $role->save();

        LogController::insert(Auth::id(), 11, 'Role successfully created.');

        return redirect('admin/roles');
    }


    /**
     * Edit page of Roles.
     */
    public function edit($id): Factory|View|Application
    {
        $role = Role::where('id', $id)->first();

        $pageTitle = __('role::role.rolesEditSub');
        $pageSub = trans('rolesEditSub');

        return view('roles::edit', compact('pageTitle', 'pageSub', 'role'));
    }


    /**
     * Update method of Roles.
     */
    public function update(Request $request, $id)
    {
        $this->validateRole($request);

        $updateRole = Role::where('id', $id)->first();
        $updateRole->update($request->only('name_cs', 'name_en'));

        if ($updateRole->id != 1) {

            $updateRole->product_index = $request->get("product_index") == 'on' ? 1 : 0;
            $updateRole->product_create = $request->get("product_create") == 'on' ? 1 : 0;
            $updateRole->product_edit = $request->get("product_edit") == 'on' ? 1 : 0;
            $updateRole->product_delete = $request->get("product_delete") == 'on' ? 1 : 0;

            $updateRole->category_index = $request->get("category_index") == 'on' ? 1 : 0;
            $updateRole->category_create = $request->get("category_create") == 'on' ? 1 : 0;
            $updateRole->category_edit = $request->get("category_edit") == 'on' ? 1 : 0;
            $updateRole->category_delete = $request->get("category_delete") == 'on' ? 1 : 0;

            $updateRole->tag_index = $request->get("tag_index") == 'on' ? 1 : 0;
            $updateRole->tag_create = $request->get("tag_create") == 'on' ? 1 : 0;
            $updateRole->tag_edit = $request->get("tag_edit") == 'on' ? 1 : 0;
            $updateRole->tag_delete = $request->get("tag_delete") == 'on' ? 1 : 0;

            $updateRole->user_index = $request->get("user_index") == 'on' ? 1 : 0;
            $updateRole->user_create = $request->get("user_create") == 'on' ? 1 : 0;
            $updateRole->user_edit = $request->get("user_edit") == 'on' ? 1 : 0;
            $updateRole->user_delete = $request->get("user_delete") == 'on' ? 1 : 0;

            $updateRole->role_index = $request->get("role_index") == 'on' ? 1 : 0;
            $updateRole->role_create = $request->get("role_create") == 'on' ? 1 : 0;
            $updateRole->role_edit = $request->get("role_edit") == 'on' ? 1 : 0;
            $updateRole->role_delete = $request->get("role_delete") == 'on' ? 1 : 0;

            $updateRole->page_index = $request->get("page_index") == 'on' ? 1 : 0;
            $updateRole->page_create = $request->get("page_create") == 'on' ? 1 : 0;
            $updateRole->page_edit = $request->get("page_edit") == 'on' ? 1 : 0;
            $updateRole->page_delete = $request->get("page_delete") == 'on' ? 1 : 0;

            $updateRole->report_index = $request->get("report_index") == 'on' ? 1 : 0;

            $updateRole->import_index = $request->get("import_index") == 'on' ? 1 : 0;
            $updateRole->import_create = $request->get("import_create") == 'on' ? 1 : 0;

            $updateRole->setting_index = $request->get("setting_index") == 'on' ? 1 : 0;
            $updateRole->log_index = $request->get("log_index") == 'on' ? 1 : 0;
        }

        $updateRole->save();
        LogController::insert(Auth::id(), 11, 'Role (' . $updateRole->id . ') updated.');

        return redirect('admin/roles');
    }


    /**
     * Delete method of Roles.
     */
    public function destroy($idRole)
    {
        $deleteRole = Role::where('id', $idRole)->first();

        if($deleteRole) {
            $deleteRole->delete();
        }
        LogController::insert(Auth::id(), 11, 'Role ID '. $deleteRole->id .' successfully deleted.');

        return redirect()->back();
    }

    private function validateRole(Request $request): array
    {
        $rules = [
            'name_cs' => 'required',
            'name_en' => 'required',
        ];
        return $request->validate($rules);
    }

}
