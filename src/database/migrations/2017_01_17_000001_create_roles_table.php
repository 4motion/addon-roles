<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {

            $table->id();
            $table->string('name_cs'); // name of the role
            $table->string('name_en'); // name of the role

//          Role permissions
            $table->boolean("product_index")->default(false);
            $table->boolean("product_create")->default(false);
            $table->boolean("product_edit")->default(false);
            $table->boolean("product_delete")->default(false);

            $table->boolean("category_index")->default(false);
            $table->boolean("category_create")->default(false);
            $table->boolean("category_edit")->default(false);
            $table->boolean("category_delete")->default(false);

            $table->boolean("tag_index")->default(false);
            $table->boolean("tag_create")->default(false);
            $table->boolean("tag_edit")->default(false);
            $table->boolean("tag_delete")->default(false);

            $table->boolean("report_index")->default(false);

            $table->boolean("user_index")->default(false);
            $table->boolean("user_create")->default(false);
            $table->boolean("user_edit")->default(false);
            $table->boolean("user_delete")->default(false);

            $table->boolean("page_index")->default(false);
            $table->boolean("page_create")->default(false);
            $table->boolean("page_edit")->default(false);
            $table->boolean("page_delete")->default(false);

            $table->boolean("role_index")->default(false);
            $table->boolean("role_create")->default(false);
            $table->boolean("role_edit")->default(false);
            $table->boolean("role_delete")->default(false);

            $table->boolean("import_index")->default(false);
            $table->boolean("import_create")->default(false);

            $table->boolean("setting_index")->default(false);
            $table->boolean("log_index")->default(false);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
