<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Role translations
    |--------------------------------------------------------------------------
    */

    'rolesTitle' => 'Role',
    'rolesIndexSub' => 'Seznam rolí',
    'rolesCreateSub' => 'Vytvoření role',
    'rolesEditSub' => 'Editace role',

];
