<?php

namespace Onyx\Role\Models;

use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Onyx\Product\Models\ProductCategory;
use Onyx\User\Models\User;

/**
 * Onyx\Role\Models\Role
 *
 * @method static create(array $data): Onyx\Role\Models\Role
 * @property int $id
 * @property string $name_en
 * @property string $name_cs
 * @mixin Eloquent
 */
class Role extends Model
{

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = ['name_en', 'name_cs', 'product_index', 'product_create', 'product_edit', 'product_delete', 'tag_index', 'tag_create', 'tag_edit', 'tag_delete', 'category_index', 'category_create', 'category_edit', 'category_delete', 'report_index', 'user_index', 'user_create', 'user_edit', 'user_delete', 'page_index', 'page_create', 'page_edit', 'page_delete', 'import_index', 'import_create', 'log_index', 'setting_index'];


    /**
     * Relationship to users.
     *
     * @return BelongsToMany
     */
    public function user(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }
}
