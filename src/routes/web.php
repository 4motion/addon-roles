<?php

use Illuminate\Support\Facades\Route;

Route::resource('roles', Onyx\Role\RoleController::class);
Route::post('ajaxDeleteRole', 'Onyx\Role\RoleController@ajaxDeleteRole');


Route::prefix('api')->group(function () {

    // display all roles
    Route::get('role', [Onyx\Role\Api\RoleController::class, 'index']);

    // insert new role
    Route::post('role/create', [Onyx\Role\Api\RoleController::class, 'insert']);

    // display specific role by roleId
    Route::get('role/{roleId}', [Onyx\Role\Api\RoleController::class, 'get']);

    // update specific role by roleId
    Route::put('role/{roleId}', [Onyx\Role\Api\RoleController::class, 'update']);

    // delete specific role by roleId
    Route::delete('role/{roleId}', [Onyx\Role\Api\RoleController::class, 'delete']);
});
