# Role Package [onyx/roles]

---

## Required packages
1. Menu package (onyx/menu)

---

## Installation
1. Add package to project composer - `composer require onyx/roles`
2. Publish files from package - `php artisan vendor:publish`

