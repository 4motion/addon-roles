<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Role translations
    |--------------------------------------------------------------------------
    */

    'rolesTitle' => 'Role',
    'rolesIndexSub' => 'Select Roles',
    'rolesCreateSub' => 'Create Role',
    'rolesEditSub' => 'Edit Role',

];
