<?php

namespace Onyx\Role\Api;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Onyx\Api\ApiController;
use Onyx\Log\LogController;
use Onyx\Role\Models\Resources\RoleResource;
use Onyx\Role\Models\Role;

class RoleController extends ApiController
{

    /**
     * Return all Roles as a json data
     *
     * @param Request $request
     * @return JsonResponse OK Json Response if authorized
     */
    public function index(Request $request): JsonResponse
    {

        $roles = Role::all();

        $data = ['roles' => RoleResource::collection($roles)];


        return $this->respondOk($data, 'All Roles.');
    }


    /**
     * Return specific Roles by roleId
     *
     * @param Request $request Data contains token
     * @param int $roleId Role to return
     * @return JsonResponse OK Json Response if authorized and exists
     */
    public function get(Request $request, int $roleId): JsonResponse
    {

        $role = Role::where('id', $roleId)->first();

        if (!$role) {
            return $this->respondNotFound('Role not found.');
        }

        $roleResource = new RoleResource($role);
        return $this->respondOk(['role' => $roleResource], 'Role found.');
    }


    /**
     * Create a new Role via API.
     *
     * @param Request $request Data contains token
     * @return JsonResponse OK Json Response if authorized
     */
    public function insert(Request $request): JsonResponse
    {

        if (!$this->getLoggedUser($request)) {
            return $this->respondUnauthorized();
        }

        $data = $request->only('name_cs', 'name_en', 'product_index', 'product_edit', 'product_delete', 'category_index', 'category_create', 'category_edit', 'category_delete', 'tag_index', 'tag_create', 'tag_edit', 'tag_delete', 'report_index', 'user_index', 'user_create', 'user_edit', 'user_delete', 'page_index', 'page_create', 'page_edit', 'page_delete', 'role_index', 'role_create', 'role_edit', 'role_delete', 'import_index', 'import_create', 'setting_index', 'log_index');

        $role = Role::create($data);
        $roleResource = new RoleResource($role);

        LogController::insert($this->getLoggedUser($request)->id, 11, 'Role successfully created.');

        return $this->respondOk(['role' => $roleResource], 'Role successfully created.');
    }


    /**
     * Update given Role
     *
     * @param Request $request Role data and token
     * @param int $roleId Role to update
     * @return JsonResponse OK Json Response if authorized and exists
     */
    public function update(Request $request, int $roleId): JsonResponse
    {

        if (!$this->getLoggedUser($request)) {
            return $this->respondUnauthorized();
        }

        $role = Role::where('id', $roleId)->first();

        if (!$role) {
            return $this->respondNotFound('Role for update not found.');
        }

        $data = $request->only('name_cs', 'name_en', 'product_index', 'product_edit', 'product_delete', 'category_index', 'category_create', 'category_edit', 'category_delete', 'tag_index', 'tag_create', 'tag_edit', 'tag_delete', 'report_index', 'user_index', 'user_create', 'user_edit', 'user_delete', 'page_index', 'page_create', 'page_edit', 'page_delete', 'role_index', 'role_create', 'role_edit', 'role_delete', 'import_index', 'import_create', 'setting_index', 'log_index');

        if ($role->update($data)) {

            $roleResource = new RoleResource($role);

            LogController::insert($this->getLoggedUser($request)->id, 11, 'Role (' . $role->id . ') updated.');
            return $this->respondOk(['role' => $roleResource], 'Role (ID ' . $role->id . ') updated.');

        } else {

            LogController::insert($this->getLoggedUser($request)->id, 11, 'Role (' . $role->id . ') update error.');
            return $this->respondInternalError('Role (ID ' . $role->id . ') update error.');
        }
    }


    /**
     * Delete given Role
     *
     * @param Request $request Data contains token
     * @param int $roleId Role to delete
     * @return JsonResponse OK Json Response if authorized and Role exists
     */
    public function delete(Request $request, int $roleId): JsonResponse
    {

        if (!$this->getLoggedUser($request)) {
            return $this->respondUnauthorized();
        }

        $role = Role::where('id', $roleId)->first();

        if (!$role) {
            return $this->respondNotFound('Role to delete not found.');
        }

        try {
            $role->delete();

            LogController::insert($this->getLoggedUser($request)->id, 11, 'Role ID ' . $role->id . ' successfully deleted.');
            return $this->respondOk(['success' => 'Role deleted.'], 'Role ID ' . $role->id . ' successfully deleted.');
        } catch (\Exception $e) {

            LogController::insert($this->getLoggedUser($request)->id, 11, 'Role ID ' . $role->id . ' delete error.');
            return $this->respondInternalError('Role ID ' . $role->id . ' delete error.');
        }

    }


}
