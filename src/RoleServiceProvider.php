<?php

namespace Onyx\Role;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\ServiceProvider;

class RoleServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     * @throws BindingResolutionException
     */
    public function register()
    {
        $this->app->make('Onyx\Role\RoleController');
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'roles');
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        $this->loadTranslationsFrom(__DIR__ . '/resources/lang', 'role');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__ . '/routes/web.php';

        $this->loadTranslationsFrom(__DIR__.'/resources/lang', 'role');

        $this->publishes([
            __DIR__ . '/config/role.php' => config_path('role.php'),
            __DIR__ . '/config/menu.txt' => config_path('menu-items/role.txt'),
            __DIR__ . '/resources/lang' => resource_path('lang/role'),
            __DIR__ . '/resources/views' => resource_path('views/roles'),
            __DIR__ . '/database/seeders' => database_path('seeders'),
        ]);
    }
}
