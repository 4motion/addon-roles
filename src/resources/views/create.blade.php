@section('page_title')
    {{ $pageTitle .' |' }}
@endsection
<x-base-layout>

    @if(\Illuminate\Support\Facades\Auth::user()->role->role_index == 0)
        <div class="card d-flex justify-content-center" style="width: 100%;height: 100%;">
            <div style="margin: 0 auto;" class="d-flex flex-column justify-content-center">
                <img src="{{ asset(theme()->getMediaUrlPath() . 'logos/logo.png') }}" width="300" alt=""
                     style="margin: auto !important;">
                <h2 class="fw-boldest text-gray-700 mt-5">Nemáte oprávnění k zobrazení této strany!</h2>
            </div>
        </div>
    @else

        <form method="POST" class="form" action="{{ route('roles.store') }}" enctype="multipart/form-data">
            @csrf
            <div class="card shadow">

                <div class="card-header d-flex justify-content-between py-5 px-5">
                    <a href="{{ route('roles.index') }}" class="btn btn-secondary font-weight-bolder shadow-sm">
                        <i class="fa fa-arrow-circle-left"></i> zpět
                    </a>
                    <div class="mt-4"><h2 class="text-gray-700">Nová uživatelská role</h2></div>
                    <button type="submit" class="btn btn-primary font-weight-bolder shadow-sm btn-hover-scale"
                            name="submitForm" id="submitForm">
                        <i class="fa fa-save fw-boldest" id="btnSave"></i>
                        <i class="fa fa-check d-none fw-boldest" id="btnSaveCheck"></i>
                        Uložit
                    </button>
                </div>

                <div class="card-body px-0">
                    <div class="tab-content pt-5">
                        <div class="tab-pane active" id="kt_apps_contacts_view_tab_2" role="tabpanel">
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <label class="col-lg-3 col-form-label fw-boldest text-gray-700 required"
                                           style="text-align: right;">Název CZ</label>
                                    <div class="col-lg-6">
                                        <input type="text" name="name_cs" class="form-control form-control-solid"
                                               value="{{ old('name_cs') }}"
                                               placeholder="Zadejte název role v anglickém jazyce">
                                        @error('name_cs')
                                        <span class="text-danger">Vyplňte prosím pole název v českém jazyce</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-10">
                                    <label class="col-lg-3 col-form-label fw-boldest text-gray-700 required"
                                           style="text-align: right;">Název EN</label>
                                    <div class="col-lg-6">
                                        <input type="text" name="name_en" class="form-control form-control-solid"
                                               value="{{ old('name_en') }}"
                                               placeholder="Zadejte název role v českém jazyce">
                                        @error('name_en')
                                        <span class="text-danger">Vyplňte prosím pole název v anglickém jazyce</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card shadow mt-5">

                <div class="card-header py-10">
                    <h3 class="text-gray-700">Oprávnění role</h3>
                </div>

                <div class="d-flex justify-content-center pt-10">

                    <div class="p-5 w-25">

                    <span class="d-inline-block position-relative ms-2">
                        <span class="d-inline-block mb-2 fw-bolder fs-2 text-gray-700">
                            Modul Předměty
                        </span>
                        <span
                            class="d-inline-block position-absolute h-2px bottom-0 end-0 start-0 bg-secondary translate rounded"></span>
                    </span>


                        <div class="card-body px-0">
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <div class="col-lg-12">
                                        <div class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input productCheck" type="checkbox" value="on" id="productIndex"
                                                   name="product_index" @if(count($errors) > 0 && old('product_index') == 'on' || count($errors) == 0) checked="checked" @endif/>
                                            <label class="form-check-label fw-bolder text-gray-700" for="productIndex">
                                                Zobrazit předměty
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <div class="col-lg-12">
                                        <div class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input productCheck" type="checkbox" value="on"
                                                   id="productCreate" name="product_create"  @if(count($errors) > 0 && old('product_create') == 'on') checked="checked" @endif/>
                                            <label class="form-check-label fw-bolder text-gray-700" for="productCreate">
                                                Vytvořit předmět
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <div class="col-lg-12">
                                        <div class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input productCheck" type="checkbox" value="on" id="productEdit"
                                                   name="product_edit"  @if(count($errors) > 0 && old('product_edit') == 'on') checked="checked" @endif/>
                                            <label class="form-check-label fw-bolder text-gray-700" for="productEdit">
                                                Upravit předmět
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <div class="col-lg-12">
                                        <div class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input productCheck" type="checkbox" value="on"
                                                   id="productDelete" name="product_delete" @if(count($errors) > 0 && old('product_delete') == 'on') checked="checked" @endif/>
                                            <label class="form-check-label fw-bolder text-gray-700" for="productDelete">
                                                Smazat předmět
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="p-5 w-25">

                    <span class="d-inline-block position-relative ms-2">
                        <span class="d-inline-block mb-2 fw-bolder fs-2 text-gray-700">
                            Modul kategorie
                        </span>
                        <span
                            class="d-inline-block position-absolute h-2px bottom-0 end-0 start-0 bg-secondary translate rounded"></span>
                    </span>

                        <div class="card-body px-0">
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <div class="col-lg-12">
                                        <div class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="on"
                                                   id="categoryIndex" name="category_index"  @if(count($errors) > 0 && old('category_index') == 'on' || count($errors) == 0) checked="checked" @endif/>
                                            <label class="form-check-label fw-bolder text-gray-700" for="categoryIndex">
                                                Zobrazit Kategorie
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <div class="col-lg-12">
                                        <div class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="on"
                                                   id="categoryCreate" name="category_create"  @if(count($errors) > 0 && old('category_create') == 'on') checked="checked" @endif/>
                                            <label class="form-check-label fw-bolder text-gray-700"
                                                   for="categoryCreate">
                                                Vytvořit kategorii
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <div class="col-lg-12">
                                        <div class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="on" id="categoryEdit"
                                                   name="category_edit" @if(count($errors) > 0 && old('category_edit') == 'on') checked="checked" @endif/>
                                            <label class="form-check-label fw-bolder text-gray-700" for="categoryEdit">
                                                Upravit kategorii
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <div class="col-lg-12">
                                        <div class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="on"
                                                   id="categoryDelete" name="category_delete"  @if(count($errors) > 0 && old('category_delete') == 'on') checked="checked" @endif/>
                                            <label class="form-check-label fw-bolder text-gray-700"
                                                   for="categoryDelete">
                                                Smazat kategorii
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="p-5 w-25">

                    <span class="d-inline-block position-relative ms-2">
                        <span class="d-inline-block mb-2 fw-bolder fs-2 text-gray-700">
                            Modul Tagy
                        </span>
                        <span
                            class="d-inline-block position-absolute h-2px bottom-0 end-0 start-0 bg-secondary translate rounded"></span>
                    </span>

                        <div class="card-body px-0">
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <div class="col-lg-12">
                                        <div class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="on" id="tagIndex"
                                                   name="tag_index"  @if(count($errors) > 0 && old('tag_index') == 'on' || count($errors) == 0) checked="checked" @endif/>
                                            <label class="form-check-label fw-bolder text-gray-700" for="tagIndex">
                                                Zobrazit tagy
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <div class="col-lg-12">
                                        <div class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="on" id="tagCreate"
                                                   name="tag_create" @if(count($errors) > 0 && old('tag_create') == 'on') checked="checked" @endif/>
                                            <label class="form-check-label fw-bolder text-gray-700" for="tagCreate">
                                                Vytvořit tag
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <div class="col-lg-12">
                                        <div class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="on" id="tagEdit"
                                                   name="tag_edit"  @if(count($errors) > 0 && old('tag_edit') == 'on') checked="checked" @endif/>
                                            <label class="form-check-label fw-bolder text-gray-700" for="tagEdit">
                                                Upravit tag
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <div class="col-lg-12">
                                        <div class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="on" id="tagDelete"
                                                   name="tag_delete" @if(count($errors) > 0 && old('tag_delete') == 'on') checked="checked" @endif/>
                                            <label class="form-check-label fw-bolder text-gray-700" for="tagDelete">
                                                Smazat tag
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="d-flex justify-content-center pt-10">

                    <div class="p-5 w-25">

                    <span class="d-inline-block position-relative ms-2">
                        <span class="d-inline-block mb-2 fw-bolder fs-2 text-gray-700">
                            Modul Uživatelé
                        </span>
                        <span
                            class="d-inline-block position-absolute h-2px bottom-0 end-0 start-0 bg-secondary translate rounded"></span>
                    </span>


                        <div class="card-body px-0">
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <div class="col-lg-12">
                                        <div class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="on" id="userIndex"
                                                   name="user_index" @if(count($errors) > 0 && old('user_index') == 'on' || count($errors) == 0) checked="checked" @endif/>
                                            <label class="form-check-label fw-bolder text-gray-700" for="userIndex">
                                                Zobrazit uživatele
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <div class="col-lg-12">
                                        <div class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="on" id="userCreate"
                                                   name="user_create" @if(count($errors) > 0 && old('user_create') == 'on') checked="checked" @endif/>
                                            <label class="form-check-label fw-bolder text-gray-700" for="userCreate">
                                                Vytvořit uživatele
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <div class="col-lg-12">
                                        <div class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="on" id="userEdit"
                                                   name="user_edit" @if(count($errors) > 0 && old('user_edit') == 'on') checked="checked" @endif/>
                                            <label class="form-check-label fw-bolder text-gray-700" for="userEdit">
                                                Upravit uživatele
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <div class="col-lg-12">
                                        <div class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="on" id="userDelete"
                                                   name="user_delete" @if(count($errors) > 0 && old('user_delete') == 'on') checked="checked" @endif/>
                                            <label class="form-check-label fw-bolder text-gray-700" for="userDelete">
                                                Smazat uživatele
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="p-5 w-25">

                    <span class="d-inline-block position-relative ms-2">
                        <span class="d-inline-block mb-2 fw-bolder fs-2 text-gray-700">
                            Modul Role
                        </span>
                        <span
                            class="d-inline-block position-absolute h-2px bottom-0 end-0 start-0 bg-secondary translate rounded"></span>
                    </span>


                        <div class="card-body px-0">
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <div class="col-lg-12">
                                        <div class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="on" id="roleIndex"
                                                   name="role_index" @if(count($errors) > 0 && old('role_index') == 'on' || count($errors) == 0) checked="checked" @endif/>
                                            <label class="form-check-label fw-bolder text-gray-700" for="roleIndex">
                                                Zobrazit role
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <div class="col-lg-12">
                                        <div class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="on" id="roleCreate"
                                                   name="role_create" @if(count($errors) > 0 && old('role_create') == 'on') checked="checked" @endif/>
                                            <label class="form-check-label fw-bolder text-gray-700" for="roleCreate">
                                                Vytvořit roli
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <div class="col-lg-12">
                                        <div class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="on" id="roleEdit"
                                                   name="role_edit" @if(count($errors) > 0 && old('role_edit') == 'on') checked="checked" @endif/>
                                            <label class="form-check-label fw-bolder text-gray-700" for="roleEdit">
                                                Upravit roli
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <div class="col-lg-12">
                                        <div class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="on" id="roleDelete"
                                                   name="role_delete" @if(count($errors) > 0 && old('role_delete') == 'on') checked="checked" @endif/>
                                            <label class="form-check-label fw-bolder text-gray-700" for="roleDelete">
                                                Smazat roli
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="p-5 w-25">

                    <span class="d-inline-block position-relative ms-2">
                        <span class="d-inline-block mb-2 fw-bolder fs-2 text-gray-700">
                            Modul Správa obsahu
                        </span>
                        <span
                            class="d-inline-block position-absolute h-2px bottom-0 end-0 start-0 bg-secondary translate rounded"></span>
                    </span>


                        <div class="card-body px-0">
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <div class="col-lg-12">
                                        <div class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="on" id="pageIndex"
                                                   name="page_index" @if(count($errors) > 0 && old('page_index') == 'on' || count($errors) == 0) checked="checked" @endif/>
                                            <label class="form-check-label fw-bolder text-gray-700" for="pageIndex">
                                                Zobrazit strany
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <div class="col-lg-12">
                                        <div class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="on" id="pageCreate"
                                                   name="page_create" @if(count($errors) > 0 && old('page_create') == 'on') checked="checked" @endif/>
                                            <label class="form-check-label fw-bolder text-gray-700" for="pageCreate">
                                                Vytvořit stranu
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <div class="col-lg-12">
                                        <div class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="on" id="pageEdit"
                                                   name="page_edit" @if(count($errors) > 0 && old('page_edit') == 'on') checked="checked" @endif/>
                                            <label class="form-check-label fw-bolder text-gray-700" for="pageEdit">
                                                Upravit stranu
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <div class="col-lg-12">
                                        <div class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="on" id="pageDelete"
                                                   name="page_delete" @if(count($errors) > 0 && old('page_delete') == 'on') checked="checked" @endif/>
                                            <label class="form-check-label fw-bolder text-gray-700" for="pageDelete">
                                                Smazat stranu
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="d-flex justify-content-center pt-10">

                    <div class="p-5 w-25">

                    <span class="d-inline-block position-relative ms-2">
                        <span class="d-inline-block mb-2 fw-bolder fs-2 text-gray-700">
                            Modul reporty
                        </span>
                        <span
                            class="d-inline-block position-absolute h-2px bottom-0 end-0 start-0 bg-secondary translate rounded"></span>
                    </span>


                        <div class="card-body px-0">
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <div class="col-lg-12">
                                        <div class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="on" id="reportIndex"
                                                   name="report_index" @if(count($errors) > 0 && old('report_index') == 'on' || count($errors) == 0) checked="checked" @endif/>
                                            <label class="form-check-label fw-bolder text-gray-700" for="reportIndex">
                                                Zobrazit reporty
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--                        <div class="form-group row">--}}
                            {{--                            <div class="form-group row mb-10">--}}
                            {{--                                <div class="col-lg-12">--}}
                            {{--                                    <div class="form-check form-switch form-check-custom form-check-solid">--}}
                            {{--                                        <input class="form-check-input" type="checkbox" value="" id="report" checked="checked" />--}}
                            {{--                                        <label class="form-check-label" for="flexSwitchChecked">--}}
                            {{--                                            Report předmětů--}}
                            {{--                                        </label>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            {{--                        </div>--}}
                            {{--                        <div class="form-group row">--}}
                            {{--                            <div class="form-group row mb-10">--}}
                            {{--                                <div class="col-lg-12">--}}
                            {{--                                    <div class="form-check form-switch form-check-custom form-check-solid">--}}
                            {{--                                        <input class="form-check-input" type="checkbox" value="" id="flexSwitchChecked" checked="checked" />--}}
                            {{--                                        <label class="form-check-label" for="flexSwitchChecked">--}}
                            {{--                                            Report 2..--}}
                            {{--                                        </label>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            {{--                        </div>--}}
                            {{--                        <div class="form-group row">--}}
                            {{--                            <div class="form-group row mb-10">--}}
                            {{--                                <div class="col-lg-12">--}}
                            {{--                                    <div class="form-check form-switch form-check-custom form-check-solid">--}}
                            {{--                                        <input class="form-check-input" type="checkbox" value="" id="flexSwitchChecked" checked="checked" />--}}
                            {{--                                        <label class="form-check-label" for="flexSwitchChecked">--}}
                            {{--                                            Report 3..--}}
                            {{--                                        </label>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            {{--                        </div>--}}
                        </div>
                    </div>

                    <div class="p-5 w-25">

                    <span class="d-inline-block position-relative ms-2">
                        <span class="d-inline-block mb-2 fw-bolder fs-2 text-gray-700">
                            Modul Import
                        </span>
                        <span
                            class="d-inline-block position-absolute h-2px bottom-0 end-0 start-0 bg-secondary translate rounded"></span>
                    </span>


                        <div class="card-body px-0">
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <div class="col-lg-12">
                                        <div class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="on" id="importIndex"
                                                   name="import_index" @if(count($errors) > 0 && old('import_index') == 'on') checked="checked" @endif/>
                                            <label class="form-check-label fw-bolder text-gray-700" for="importIndex">
                                                Zobrazit importy
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <div class="col-lg-12">
                                        <div class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="on" id="importCreate"
                                                   name="import_create" @if(count($errors) > 0 && old('import_create') == 'on') checked="checked" @endif/>
                                            <label class="form-check-label fw-bolder text-gray-700" for="importCreate">
                                                Manuální import
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="p-5 w-25">

                    <span class="d-inline-block position-relative ms-2">
                        <span class="d-inline-block mb-2 fw-bolder fs-2 text-gray-700">
                            Modul Nastavení
                        </span>
                        <span
                            class="d-inline-block position-absolute h-2px bottom-0 end-0 start-0 bg-secondary translate rounded"></span>
                    </span>


                        <div class="card-body px-0">
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <div class="col-lg-12">
                                        <div class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="on" id="settingIndex"
                                                   name="setting_index" @if(count($errors) > 0 && old('setting_index') == 'on' || count($errors) == 0) checked="checked" @endif/>
                                            <label class="form-check-label fw-bolder text-gray-700" for="settingIndex">
                                                Nastavení aplikace
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="form-group row mb-10">
                                    <div class="col-lg-12">
                                        <div class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" value="on" id="logIndex"
                                                   name="log_index" @if(count($errors) > 0 && old('log_index') == 'on' || count($errors) == 0) checked="checked" @endif/>
                                            <label class="form-check-label fw-bolder text-gray-700" for="logIndex">
                                                Systémový log
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    @endif

</x-base-layout>

<script>

    $('#submitForm').mouseenter(function () {

        $('#btnSave').addClass('d-none');
        $('#btnSaveCheck').removeClass('d-none');

    });

    $('#submitForm').mouseleave(function () {

        $('#btnSave').removeClass('d-none');
        $('#btnSaveCheck').addClass('d-none');

    });

    if(@json(count($errors) > 0)){

        if(!@json(old('product_index'))){
            $("#productCreate").attr("disabled", "disabled").val(null);
            $("#productEdit").attr("disabled", "disabled").val(null);
            $("#productDelete").attr("disabled", "disabled").val(null);
        }

        if(!@json(old('category_index'))){
            $("#categoryCreate").attr("disabled", "disabled").val(null);
            $("#categoryEdit").attr("disabled", "disabled").val(null);
            $("#categoryDelete").attr("disabled", "disabled").val(null);
        }

        if(!@json(old('tag_index'))){
            $("#tagCreate").attr("disabled", "disabled").val(null);
            $("#tagEdit").attr("disabled", "disabled").val(null);
            $("#tagDelete").attr("disabled", "disabled").val(null);
        }

        if(!@json(old('user_index'))){
            $("#userCreate").attr("disabled", "disabled").val(null);
            $("#userEdit").attr("disabled", "disabled").val(null);
            $("#userDelete").attr("disabled", "disabled").val(null);
        }

        if(!@json(old('role_index'))){
            $("#roleCreate").attr("disabled", "disabled").val(null);
            $("#roleEdit").attr("disabled", "disabled").val(null);
            $("#roleDelete").attr("disabled", "disabled").val(null);
        }

        if(!@json(old('page_index'))){
            $("#pageCreate").attr("disabled", "disabled").val(null);
            $("#pageEdit").attr("disabled", "disabled").val(null);
            $("#pageDelete").attr("disabled", "disabled").val(null);
        }

        if(!@json(old('import_index'))){

            $("#importCreate").attr("disabled", "disabled").val(null);
        }
    }

    $("#productIndex").click(function () {

        let state = $("#productIndex").prop("checked");

        if (state === false){

            $("#productCreate").attr("disabled", "disabled").val(null);
            $("#productEdit").attr("disabled", "disabled").val(null);
            $("#productDelete").attr("disabled", "disabled").val(null);
        }else{

            $("#productCreate").removeAttr("disabled").val("on");
            $("#productEdit").removeAttr("disabled").val("on");
            $("#productDelete").removeAttr("disabled").val("on");
        }
    });

    $("#categoryIndex").click(function () {

        let state = $("#categoryIndex").prop("checked");

        if (state === false){

            $("#categoryCreate").attr("disabled", "disabled").val(null);
            $("#categoryEdit").attr("disabled", "disabled").val(null);
            $("#categoryDelete").attr("disabled", "disabled").val(null);
        }else{

            $("#categoryCreate").removeAttr("disabled").val("on");
            $("#categoryEdit").removeAttr("disabled").val("on");
            $("#categoryDelete").removeAttr("disabled").val("on");
        }
    });

    $("#tagIndex").click(function () {

        let state = $("#tagIndex").prop("checked");

        if (state === false){

            $("#tagCreate").attr("disabled", "disabled").val(null);
            $("#tagEdit").attr("disabled", "disabled").val(null);
            $("#tagDelete").attr("disabled", "disabled").val(null);
        }else{

            $("#tagCreate").removeAttr("disabled").val("on");
            $("#tagEdit").removeAttr("disabled").val("on");
            $("#tagDelete").removeAttr("disabled").val("on");
        }
    });

    $("#userIndex").click(function () {

        let state = $("#userIndex").prop("checked");

        if (state === false){

            $("#userCreate").attr("disabled", "disabled").val(null);
            $("#userEdit").attr("disabled", "disabled").val(null);
            $("#userDelete").attr("disabled", "disabled").val(null);
        }else{

            $("#userCreate").removeAttr("disabled").val("on");
            $("#userEdit").removeAttr("disabled").val("on");
            $("#userDelete").removeAttr("disabled").val("on");
        }
    });

    $("#roleIndex").click(function () {

        let state = $("#roleIndex").prop("checked");

        if (state === false){

            $("#roleCreate").attr("disabled", "disabled").val(null);
            $("#roleEdit").attr("disabled", "disabled").val(null);
            $("#roleDelete").attr("disabled", "disabled").val(null);
        }else{

            $("#roleCreate").removeAttr("disabled").val("on");
            $("#roleEdit").removeAttr("disabled").val("on");
            $("#roleDelete").removeAttr("disabled").val("on");
        }
    });

    $("#pageIndex").click(function () {

        let state = $("#pageIndex").prop("checked");

        if (state === false){

            $("#pageCreate").attr("disabled", "disabled").val(null);
            $("#pageEdit").attr("disabled", "disabled").val(null);
            $("#pageDelete").attr("disabled", "disabled").val(null);
        }else{

            $("#pageCreate").removeAttr("disabled").val("on");
            $("#pageEdit").removeAttr("disabled").val("on");
            $("#pageDelete").removeAttr("disabled").val("on");
        }
    });

    $("#reportIndex").click(function () {

        let state = $("#reportIndex").prop("checked");

        if (state === false){

            // $("#reportCreate").attr("disabled", "disabled").val(null);
            // $("#reportEdit").attr("disabled", "disabled").val(null);
            // $("#reportDelete").attr("disabled", "disabled").val(null);
        }else{

            // $("#reportCreate").removeAttr("disabled").val("on");
            // $("#reportEdit").removeAttr("disabled").val("on");
            // $("#reportDelete").removeAttr("disabled").val("on");
        }
    });

    $("#importIndex").click(function () {

        let state = $("#importIndex").prop("checked");

        if (state === false){

            $("#importCreate").attr("disabled", "disabled").val(null);
        }else{

            $("#importCreate").removeAttr("disabled").val("on");
        }
    });

</script>
