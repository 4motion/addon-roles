<?php

namespace Onyx\Role\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Onyx\Role\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $roles = array(
            array('name_cs' => 'Administrátor', 'name_en' => 'Administrator',
                'product_index' => true, 'product_create' => true, 'product_edit' => true, 'product_delete' => true,
                'tag_index' => true, 'tag_create' => true, 'tag_edit' => true, 'tag_delete' => true,
                'category_index' => true, 'category_create' => true, 'category_edit' => true, 'category_delete' => true,
                'report_index' => true,
                'user_index' => true, 'user_create' => true, 'user_edit' => true, 'user_delete' => true,
                'page_index' => true, 'page_create' => true, 'page_edit' => true, 'page_delete' => true,
                'import_index' => true, 'import_create' => true,
                'log_index' => true, 'setting_index' => true, 'role_index' => true, 'role_create' => true, 'role_edit' => true, 'role_delete' => true),

            array('name_cs' => 'Uživatel', 'name_en' => 'User',
                'product_index' => false, 'product_create' => false, 'product_edit' => false, 'product_delete' => false,
                'tag_index' => false, 'tag_create' => false, 'tag_edit' => false, 'tag_delete' => false,
                'category_index' => false, 'category_create' => false, 'category_edit' => false, 'category_delete' => false,
                'report_index' => false,
                'user_index' => false, 'user_create' => false, 'user_edit' => false, 'user_delete' => false,
                'page_index' => false, 'page_create' => false, 'page_edit' => false, 'page_delete' => false,
                'import_index' => false, 'import_create' => false,
                'log_index' => false, 'setting_index' => false, 'role_index' => false, 'role_create' => false, 'role_edit' => false, 'role_delete' => false),

            array('name_cs' => 'Kurátor', 'name_en' => 'Curator',
                'product_index' => true, 'product_create' => true, 'product_edit' => true, 'product_delete' => true,
                'tag_index' => true, 'tag_create' => true, 'tag_edit' => true, 'tag_delete' => true,
                'category_index' => true, 'category_create' => true, 'category_edit' => true, 'category_delete' => true,
                'report_index' => true,
                'user_index' => true, 'user_create' => true, 'user_edit' => true, 'user_delete' => true,
                'page_index' => true, 'page_create' => true, 'page_edit' => true, 'page_delete' => true,
                'import_index' => true, 'import_create' => true,
                'log_index' => true, 'setting_index' => true, 'role_index' => true, 'role_create' => true, 'role_edit' => true, 'role_delete' => true),

            array('name_cs' => 'Manažer', 'name_en' => 'Manager',
                'product_index' => true, 'product_create' => true, 'product_edit' => true, 'product_delete' => true,
                'tag_index' => true, 'tag_create' => true, 'tag_edit' => true, 'tag_delete' => true,
                'category_index' => true, 'category_create' => true, 'category_edit' => true, 'category_delete' => true,
                'report_index' => true,
                'user_index' => true, 'user_create' => true, 'user_edit' => true, 'user_delete' => true,
                'page_index' => true, 'page_create' => true, 'page_edit' => true, 'page_delete' => true,
                'import_index' => true, 'import_create' => true,
                'log_index' => true, 'setting_index' => true, 'role_index' => true, 'role_create' => true, 'role_edit' => true, 'role_delete' => true),
        );

        foreach ($roles as $key => $value) {

            $roleModel = new Role();
            $roleModel->setAttribute('name_cs', $value['name_cs']);
            $roleModel->setAttribute('name_en', $value['name_en']);

            $roleModel->setAttribute('product_index', $value['product_index']);
            $roleModel->setAttribute('product_create', $value['product_create']);
            $roleModel->setAttribute('product_edit', $value['product_edit']);
            $roleModel->setAttribute('product_delete', $value['product_delete']);

            $roleModel->setAttribute('tag_index', $value['tag_index']);
            $roleModel->setAttribute('tag_create', $value['tag_create']);
            $roleModel->setAttribute('tag_edit', $value['tag_edit']);
            $roleModel->setAttribute('tag_delete', $value['tag_delete']);

            $roleModel->setAttribute('category_index', $value['category_index']);
            $roleModel->setAttribute('category_create', $value['category_create']);
            $roleModel->setAttribute('category_edit', $value['category_edit']);
            $roleModel->setAttribute('category_delete', $value['category_delete']);

            $roleModel->setAttribute('report_index', $value['report_index']);

            $roleModel->setAttribute('user_index', $value['user_index']);
            $roleModel->setAttribute('user_create', $value['user_create']);
            $roleModel->setAttribute('user_edit', $value['user_edit']);
            $roleModel->setAttribute('user_delete', $value['user_delete']);

            $roleModel->setAttribute('page_index', $value['page_index']);
            $roleModel->setAttribute('page_create', $value['page_create']);
            $roleModel->setAttribute('page_edit', $value['page_edit']);
            $roleModel->setAttribute('page_delete', $value['page_delete']);

            $roleModel->setAttribute('role_index', $value['role_index']);
            $roleModel->setAttribute('role_create', $value['role_create']);
            $roleModel->setAttribute('role_edit', $value['role_edit']);
            $roleModel->setAttribute('role_delete', $value['role_delete']);

            $roleModel->setAttribute('import_index', $value['import_index']);
            $roleModel->setAttribute('import_create', $value['import_create']);

            $roleModel->setAttribute('log_index', $value['log_index']);
            $roleModel->setAttribute('setting_index', $value['setting_index']);


            $roleModel->save();
        }

    }

}
