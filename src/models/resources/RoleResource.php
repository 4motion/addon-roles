<?php

namespace Onyx\Role\Models\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JetBrains\PhpStorm\ArrayShape;

class RoleResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name_en' => $this->name_en,
            'name_cs' => $this->name_cs,
            'product_index' => $this->product_index,
            'product_create' => $this->product_create,
            'product_edit' => $this->product_edit,
            'product_delete' => $this->product_delete,
            'tag_index' => $this->tag_index,
            'tag_create' => $this->tag_create,
            'tag_edit' => $this->tag_edit,
            'tag_delete' => $this->tag_delete,
            'category_index' => $this->category_index,
            'category_create' => $this->category_create,
            'category_edit' => $this->category_edit,
            'category_delete' => $this->category_delete,
            'report_index' => $this->report_index,
            'user_index' => $this->user_index,
            'user_create' => $this->user_create,
            'user_edit' => $this->user_edit,
            'user_delete' => $this->user_delete,
            'page_index' => $this->page_index,
            'page_create' => $this->page_create,
            'page_edit' => $this->page_edit,
            'page_delete' => $this->page_delete,
            'import_index' => $this->import_index,
            'import_create' => $this->import_create,
            'log_index' => $this->log_index,
            'setting_index' => $this->setting_index
        ];
    }
}
